package xyz.hlh.crypto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HLH
 * @description: 启动类
 * @email 17703595860@163.com
 * @date : Created in 2022/2/4 9:15
 */
@SpringBootApplication
public class CryptoTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(CryptoTestApplication.class, args);
    }
}
