package xyz.hlh.crypto.constant;

/**
 * @author HLH
 * @description: 加密解密用到的常量
 * @email 17703595860@163.com
 * @date : Created in 2022/2/4 20:56
 */
public class CryptoConstant {

    /** 请求解密之前的数据存放key */
    public static final String INPUT_ORIGINAL_DATA = "inputOriginalData";
    /** 请求解密之后的数据存放key */
    public static final String INPUT_DECRYPT_DATA = "inputDescryptData";

}
