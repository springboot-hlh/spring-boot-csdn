package xyz.hlh.crypto.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author HLH
 * @description:
 * @email 17703595860@163.com
 * @date : Created in 2022/2/4 15:41
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class RequestData implements Serializable {

    // 加密的文本
    private String text;

}
