package xyz.hlh.boot4.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HLH
 * @description 统一的返回结果
 * @email 17703595860@163.com
 * @date Created in 2021/8/10 下午10:45
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Result<T> {

    private int status;
    private T data;
    private String errorMsg;

}
