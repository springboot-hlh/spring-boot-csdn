package xyz.hlh.boot4.common;

import org.springframework.http.HttpStatus;

import java.util.Collections;

/**
 * @author HLH
 * @description 返回值构造
 * @email 17703595860@163.com
 * @date Created in 2021/8/10 下午10:47
 */
public interface ResultBuilder {

    /**
     * 成功的构造
     * @param data 数据
     * @return Result
     */
    default Result<?> success(Object data) {
        return Result.builder()
                .status(HttpStatus.OK.value()).data(data)
                .build();
    }

    /**
     * 404的构造
     * @param errorMsg 错误信息
     * @return Result
     */
    default Result<?> notFound(String errorMsg) {
        return Result.builder()
                .status(HttpStatus.NOT_FOUND.value())
                .errorMsg(errorMsg)
                .build();
    }

    /**
     * 500的构造
     * @param errorMsg 错误信息
     * @return Result
     */
    default Result<?> internalServerError(String errorMsg) {
        return Result.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .errorMsg(errorMsg)
                .build();
    }

}
