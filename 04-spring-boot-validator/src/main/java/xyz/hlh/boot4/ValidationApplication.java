package xyz.hlh.boot4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HLH
 * @description 启动类
 * @email 17703595860@163.com
 * @date Created in 2021/8/10 下午10:15
 */
@SpringBootApplication
public class ValidationApplication {
    public static void main(String[] args) {
        SpringApplication.run(ValidationApplication.class, args);
    }
}
