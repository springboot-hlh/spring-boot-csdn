package xyz.hlh.boot4.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.hlh.boot4.common.Result;
import xyz.hlh.boot4.common.ResultBuilder;
import xyz.hlh.boot4.vo.OrderVo;

/**
 * @author HLH
 * @description 订单模拟Controller
 * @email 17703595860@163.com
 * @date 2021/8/11 上午9:46
 */
@RestController
@RequestMapping("/order")
public class OrderController implements ResultBuilder {

    @PostMapping
    public Result<?> insertOrder(@RequestBody @Validated OrderVo orderVo) {
        return success("新增订单成功");
    }

}
