package xyz.hlh.boot4.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import xyz.hlh.boot4.common.Result;
import xyz.hlh.boot4.common.ResultBuilder;
import xyz.hlh.boot4.validation.groups.user.UpdateUser;
import xyz.hlh.boot4.vo.LoginUserVo;
import xyz.hlh.boot4.vo.UserVo;

/**
 * @author HLH
 * @description 用户模拟接口
 * @email 17703595860@163.com
 * @date Created in 2021/8/10 下午10:20
 */
@RestController
@RequestMapping("/user")
public class UserController implements ResultBuilder {

    /**
     * 加上@Validated 注解，会自动对LoginUserVo中加了注解的字段进行校验
     */
    @PostMapping("/login")
    public Result<?> login(@RequestBody @Validated LoginUserVo loginUserVo) {
        return success("登录成功");
    }

    /**
     * 新增用户，使用默认的分组校验
     */
    @PostMapping
    public Result<?> insertUser(@RequestBody @Validated UserVo userVo) {
        return success("新增成功");
    }

    /**
     * 更新用户时使用 更新用户 的校验分组
     *  包含分组内的校验规则和默认的校验规则
     */
    @PutMapping
    public Result<?> updateUser(@RequestBody @Validated(UpdateUser.class) UserVo userVo) {
        return success("新增成功");
    }

}
