package xyz.hlh.boot4.validation.groups.user;

import javax.validation.groups.Default;

/**
 * @author HLH
 * @description 更新用户使用的分组
 * @email 17703595860@163.com
 * @date 2021/8/11 上午9:23
 */
public interface UpdateUser extends Default {
}
