package xyz.hlh.boot4.validation.annos;

import xyz.hlh.boot4.validation.validators.PasswordPattenValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotBlank;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author HLH
 * @description 密码校验注解 模仿@NotBlank写就可以
 * @email 17703595860@163.com
 * @date 2021/8/11 上午10:01
 */
@Documented
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Repeatable(PasswordPatten.List.class)
@Constraint(validatedBy = {PasswordPattenValidator.class})
public @interface PasswordPatten {

    String message() default "密码格式错误，必须位4-16位，并且包含数字，大写字母，小写字母，特殊字符";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        PasswordPatten[] value();
    }

}
