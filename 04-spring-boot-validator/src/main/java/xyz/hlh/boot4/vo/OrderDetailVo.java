package xyz.hlh.boot4.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author HLH
 * @description 订单详情vo
 * @email 17703595860@163.com
 * @date 2021/8/11 上午9:43
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetailVo implements Serializable {

    private static final long serialVersionUID = 8529818233485384618L;

    private String id;
    private String orderId;


    @NotBlank(message = "订单中商品id不能为空")
    private String goodsId;
    @NotBlank(message = "订单中商品名称不能为空")
    private String goodsName;
    private String goodsPrice;

}
