package xyz.hlh.boot4.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import xyz.hlh.boot4.validation.groups.user.UpdateUser;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author HLH
 * @description 用户vo，用于新增和修改用户
 * @email 17703595860@163.com
 * @date 2021/8/11 上午9:22
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserVo implements Serializable {

    private static final long serialVersionUID = 5233695189127942918L;

    @NotBlank(message = "更新记录id不能为空", groups = UpdateUser.class) // 更新是保证ID不为空，根据ID更新
    private String id;
    @NotBlank(message = "用户名不能为空")
    private String username;
    @NotBlank(message = "密码不能为空")
    private String password;
    @NotBlank(message = "昵称不能为空", groups = UpdateUser.class) // 更新时保证nickName不为空
    private String nickName;

}
