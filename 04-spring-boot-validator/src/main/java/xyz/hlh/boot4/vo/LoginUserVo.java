package xyz.hlh.boot4.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import xyz.hlh.boot4.validation.annos.PasswordPatten;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author HLH
 * @description 登录接口需要的用户名密码封装的vo
 * @email 17703595860@163.com
 * @date Created in 2021/8/10 下午10:18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginUserVo implements Serializable {

    private static final long serialVersionUID = -5331320733431220933L;

    @NotBlank(message = "用户名不能为空")   // 非空,message为错误的提示信息
    private String username;
    @NotBlank(message = "密码不能为空")   // 非空
    @PasswordPatten // 密码自定义校验
    private String password;

}
