package xyz.hlh.boot4.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * @author HLH
 * @description 订单vo
 * @email 17703595860@163.com
 * @date 2021/8/11 上午9:37
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderVo implements Serializable {
    private static final long serialVersionUID = 8015529308743266490L;

    private String id;
    @NotBlank(message = "订单名称不能为空")
    private String orderName;
    private Double price;

    @Valid
    private List<OrderDetailVo> orderDetailVoList;

}
