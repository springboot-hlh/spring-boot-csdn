package xyz.hlh.boot4.exception;

import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import xyz.hlh.boot4.common.Result;
import xyz.hlh.boot4.common.ResultBuilder;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HLH
 * @description 统一异常处理类
 * @email 17703595860@163.com
 * @date Created in 2021/8/10 下午10:56
 */
@RestControllerAdvice  // 返回json
public class GlobExceptionHandler implements ResultBuilder {

    @ExceptionHandler(value = BindException.class)
    public Result<?> bindExceptionHandler(BindException ex) {
        // 获取所有错误信息，拼接
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        String errorMsg = fieldErrors.stream()
                .map(fieldError -> fieldError.getField() + ":" + fieldError.getDefaultMessage())
                .collect(Collectors.joining(","));
        // 返回统一处理类
        return internalServerError(errorMsg);
    }

}
