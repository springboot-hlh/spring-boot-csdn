package xyz.hlh.boot2.poitest.eventusermodel;

import org.junit.Test;
import xyz.hlh.boot2.poitest.eventusermodel.ExcelEventUserModelParse;
import xyz.hlh.boot2.poitest.eventusermodel.SimpleSheetContentsHandler;

import java.io.IOException;

/**
 * @author HLH
 * @description EventUserModel测试
 * @email 17703595860@163.com
 * @date : Created in  2021/8/4 下午10:16
 */
public class EventUserModelTest {

    /**
     * EventUserModel模式可以支持大数据量导出
     *  100万数据
     *  每次处理1000条 耗时97.786s 内存1G左右
     *  每次处理10000条 耗时107.24s 内存1.3G左右
     */
    @Test
    public void test() {
        long start = System.currentTimeMillis();
        new ExcelEventUserModelParse("/media/hlh/13B69828E0E35204/A-IdeaProject-UOS/spring-boot-csdn/02-poi-easyExcel/src/file/07big.xlsx")
                .setHandler(new SimpleSheetContentsHandler(1000)).parse();
        long end = System.currentTimeMillis();
        System.out.println();
        System.out.println(((double) end - start) / 1000);
    }

}
