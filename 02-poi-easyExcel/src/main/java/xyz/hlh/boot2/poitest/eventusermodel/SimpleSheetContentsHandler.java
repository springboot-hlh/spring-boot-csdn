package xyz.hlh.boot2.poitest.eventusermodel;

import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.usermodel.XSSFComment;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HLH
 * @description 简单的解析器
 * @email 17703595860@163.com
 * @date : Created in  2021/8/4 下午10:16
 */
public class SimpleSheetContentsHandler implements XSSFSheetXMLHandler.SheetContentsHandler {

    /** 当前行 */
    int currRow = 0;
    /** 总记录 */
    List<List<String>> listData = new ArrayList<>();
    /** 单条临时记录 */
    List<String> rowData;
    /** 分批处理的条数 */
    private int handlerNum = 1000;

    public SimpleSheetContentsHandler(int handlerNum) {
        this.handlerNum = handlerNum;
    }

    /**
     * 开始一行的回调
     * @param rowNum 行号
     */
    @Override
    public void startRow(int rowNum) {
        // System.out.println("startRow: " + rowNum);
        rowData = new ArrayList<>();
    }

    /**
     * 结束一行的回调
     * @param rowNum 行号
     */
    @Override
    public void endRow(int rowNum) {
        // System.out.println("endRow: " + rowNum);
        // 将临时记录添加到总记录中
        listData.add(rowData);
        rowData = null;
        currRow++;
        // 如果到达指定处理数量，进行处理，清空总记录
        if (currRow % handlerNum == 0) {
            System.out.println(listData);
            listData.clear();
        }
    }

    /**
     * 当行处理完成的回调
     * @param cellReference 当前所在的单元个
     * @param formattedValue 格式化之后的字符串
     * @param comment 注释
     */
    @Override
    public void cell(String cellReference, String formattedValue, XSSFComment comment) {
        // 处理每一行中的每一列
        rowData.add(formattedValue);
    }

    @Override
    public void headerFooter(String text, boolean isHeader, String tagName) {
        System.out.println("headerFooter...");
        System.out.println(text);
        System.out.println(isHeader);
        System.out.println(tagName);
    }

}