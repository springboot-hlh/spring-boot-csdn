package xyz.hlh.boot2.poitest.usermodel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.FileOutputStream;

/**
 * @description POI简单写
 * @author HLH
 * @email 17703595860@163.com
 * @date : Created in  2021/8/2 下午9:11
 */
public class POISimpleWrite {

    private static final String PATH = "/media/hlh/13B69828E0E35204/A-IdeaProject-UOS/spring-boot-csdn/02-poi-easyExcel/src/file/";

    /**
     * xls写
     *  使用 HSSFWorkbook 进行操作
     */
    @Test
    public void poi03Write() {
        // 工作簿
        try(Workbook workbook = new HSSFWorkbook();
            FileOutputStream fileOutputStream = new FileOutputStream(PATH + "03simple.xls")) {
            // 工作表
            Sheet sheet1 = workbook.createSheet("sheet1");
            // 行
            Row row1 = sheet1.createRow(0);
            // 列
            Cell cell11 = row1.createCell(0);
            Cell cell12 = row1.createCell(1);
            // 1-1 1-2
            cell11.setCellValue("1-1");
            cell12.setCellValue("1-2");

            Row row2 = sheet1.createRow(1);
            Cell cell21 = row2.createCell(0);
            Cell cell22 = row2.createCell(1);
            // 2-1 2-2
            cell21.setCellValue("2-1");
            cell22.setCellValue("2-2");

            // 写出文件
            workbook.write(fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * xlsx 写
     *  使用 XSSFWorkbook 进行操作
     */
    @Test
    public void poi07Write() {
        // 工作簿
        try(Workbook workbook = new XSSFWorkbook();
            FileOutputStream fileOutputStream = new FileOutputStream(PATH + "07simple.xlsx")) {
            // 工作表
            Sheet sheet1 = workbook.createSheet("sheet1");
            // 行
            Row row1 = sheet1.createRow(0);
            // 列
            Cell cell11 = row1.createCell(0);
            Cell cell12 = row1.createCell(1);
            // 1-1 1-2
            cell11.setCellValue("1-1");
            cell12.setCellValue("1-2");

            Row row2 = sheet1.createRow(1);
            Cell cell21 = row2.createCell(0);
            Cell cell22 = row2.createCell(1);
            // 2-1 2-2
            cell21.setCellValue("2-1");
            cell22.setCellValue("2-2");

            // 写出文件
            workbook.write(fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * xlsx 写
     *  使用 SXSSFWorkbook 进行操作(优化之后的)
     */
    @Test
    public void poi07WriteSXSSF() {
        // 工作簿
        try(Workbook workbook = new SXSSFWorkbook();
            FileOutputStream fileOutputStream = new FileOutputStream(PATH + "07simple.xlsx")) {
            // 工作表
            Sheet sheet1 = workbook.createSheet("sheet1");
            // 行
            Row row1 = sheet1.createRow(0);
            // 列
            Cell cell11 = row1.createCell(0);
            Cell cell12 = row1.createCell(1);
            // 1-1 1-2
            cell11.setCellValue("1-1");
            cell12.setCellValue("1-2");

            Row row2 = sheet1.createRow(1);
            Cell cell21 = row2.createCell(0);
            Cell cell22 = row2.createCell(1);
            // 2-1 2-2
            cell21.setCellValue("2-1");
            cell22.setCellValue("2-2");

            // 写出文件
            workbook.write(fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
