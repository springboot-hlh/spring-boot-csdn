package xyz.hlh.boot2.poitest.eventusermodel;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.util.SAXHelper;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.model.StylesTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author HLH
 * @description EventUserModel 模式解析类
 * @email 17703595860@163.com
 * @date : Created in  2021/8/4 下午10:16
 */
public class ExcelEventUserModelParse {

    /** 文件名称 */
    private final String filename;
    /** 分批处理的条数 */
    private int handlerNum = 1000;
    /** 解析类 */
    private XSSFSheetXMLHandler.SheetContentsHandler handler;

    public ExcelEventUserModelParse(String filename) {
        this.filename = filename;
    }

    public ExcelEventUserModelParse(String filename, int handlerNum) {
        this.filename = filename;
        this.handlerNum = handlerNum;
    }

    public ExcelEventUserModelParse setHandler(XSSFSheetXMLHandler.SheetContentsHandler handler) {
        this.handler = handler;
        return this;
    }

    /**
     * 统一解析入口
     */
    public void parse() {
        OPCPackage pkg = null;
        InputStream sheetInputStream = null;
        try {
            pkg = OPCPackage.open(filename, PackageAccess.READ);
            XSSFReader xssfReader = new XSSFReader(pkg);
            StylesTable styles = xssfReader.getStylesTable();
            ReadOnlySharedStringsTable readOnlySharedStringsTable = new ReadOnlySharedStringsTable(pkg);
            sheetInputStream = xssfReader.getSheetsData().next();
            processSheet(styles, readOnlySharedStringsTable, sheetInputStream);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        } finally {
            if (sheetInputStream != null) {
                try {
                    sheetInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (pkg != null) {
                try {
                    pkg.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     *
     * @param styles
     * @param strings
     * @param sheetInputStream
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     */
    private void processSheet(StylesTable styles, ReadOnlySharedStringsTable strings, InputStream sheetInputStream)
            throws SAXException, ParserConfigurationException, IOException {
        XMLReader sheetParser = SAXHelper.newXMLReader();

        if (handler != null) {
            sheetParser.setContentHandler(new XSSFSheetXMLHandler(styles, strings, handler, false));
        } else {
            sheetParser.setContentHandler(
                    new XSSFSheetXMLHandler(styles, strings, new SimpleSheetContentsHandler(handlerNum), false));
        }

        sheetParser.parse(new InputSource(sheetInputStream));
    }

}