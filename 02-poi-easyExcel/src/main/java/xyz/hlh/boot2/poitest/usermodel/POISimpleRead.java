package xyz.hlh.boot2.poitest.usermodel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.FileInputStream;

/**
 * @description POI简单读
 * @author HLH
 * @email 17703595860@163.com
 * @date : Created in  2021/8/2 下午9:11
 */
public class POISimpleRead {

    private static final String PATH = "/media/hlh/13B69828E0E35204/A-IdeaProject-UOS/spring-boot-csdn/02-poi-easyExcel/src/file/";

    /**
     * xls读
     *  使用 HSSFWorkbook 进行操作
     */
    @Test
    public void poi03Read() {
        // 工作簿
        try(Workbook workbook = new HSSFWorkbook(new FileInputStream(PATH + "03simple.xls"))) {
            // 工作表
            Sheet sheet1 = workbook.getSheet("sheet1");
            // 行
            Row row1 = sheet1.getRow(0);
            // 列
            Cell cell11 = row1.getCell(0);
            Cell cell12 = row1.getCell(1);
            // 1-1 1-2
            System.out.println("1-1" + ": " + cell11.getStringCellValue());
            System.out.println("1-2" + ": " + cell12.getStringCellValue());

            Row row2 = sheet1.getRow(1);
            Cell cell21 = row2.getCell(0);
            Cell cell22 = row2.getCell(1);
            // 2-1 2-2
            System.out.println("2-1" + ": " + cell21.getStringCellValue());
            System.out.println("2-2" + ": " + cell22.getStringCellValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * xlsx 读
     *  使用 XSSFWorkbook 进行操作
     */
    @Test
    public void poi07Read() {
        // 工作簿
        try(Workbook workbook = new XSSFWorkbook(new FileInputStream(PATH + "07simple.xlsx"))) {
            // 工作表
            Sheet sheet1 = workbook.getSheet("sheet1");
            // 行
            Row row1 = sheet1.getRow(0);
            // 列
            Cell cell11 = row1.getCell(0);
            Cell cell12 = row1.getCell(1);
            // 1-1 1-2
            System.out.println("1-1" + ": " + cell11.getStringCellValue());
            System.out.println("1-2" + ": " + cell12.getStringCellValue());

            Row row2 = sheet1.getRow(1);
            Cell cell21 = row2.getCell(0);
            Cell cell22 = row2.getCell(1);
            // 2-1 2-2
            System.out.println("2-1" + ": " + cell21.getStringCellValue());
            System.out.println("2-2" + ": " + cell22.getStringCellValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * xlsx 读
     *  使用 SXSSFWorkbook 进行操作(优化之后的)
     */
    @Test
    public void poi07ReadSXSSF() {
        // 工作簿
        try(Workbook workbook = new XSSFWorkbook(new FileInputStream(PATH + "07simple.xlsx"))) {
            // 工作表
            Sheet sheet1 = workbook.getSheet("sheet1");
            // 行
            Row row1 = sheet1.getRow(0);
            // 列
            Cell cell11 = row1.getCell(0);
            Cell cell12 = row1.getCell(1);
            // 1-1 1-2
            System.out.println("1-1" + ": " + cell11.getStringCellValue());
            System.out.println("1-2" + ": " + cell12.getStringCellValue());

            Row row2 = sheet1.getRow(1);
            Cell cell21 = row2.getCell(0);
            Cell cell22 = row2.getCell(1);
            // 2-1 2-2
            System.out.println("2-1" + ": " + cell21.getStringCellValue());
            System.out.println("2-2" + ": " + cell22.getStringCellValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
