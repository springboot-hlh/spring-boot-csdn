package xyz.hlh.boot7.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author HLH
 * @description: 用户实体类
 * @email 17703595860@163.com
 * @date : Created in 2021/10/12 10:31
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "user", catalog = "test")
@NameStyle(value = Style.camelhumpAndLowercase)
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;
    private String email;
    private String city;
    private Boolean isDelete;

}
