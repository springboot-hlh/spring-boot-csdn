package xyz.hlh.boot7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author HLH
 * @description: 启动类
 * @email 17703595860@163.com
 * @date : Created in 2021/10/19 16:10
 */
@SpringBootApplication
@MapperScan(basePackages = "xyz.hlh.boot7.mapper")
public class MapperApplication {
    public static void main(String[] args) {
        SpringApplication.run(MapperApplication.class, args);
    }
}
