package xyz.hlh.boot7.mapper;

import tk.mybatis.mapper.common.Mapper;
import xyz.hlh.boot7.entity.User;

import java.util.List;

/**
 * @author HLH
 * @description: user曹组O接口
 * @email 17703595860@163.com
 * @date : Created in 2021/10/19 15:14
 */
public interface UserMapper extends Mapper<User> {

    /**
     * 查询所有的用户，自定义sql实现
     * @return 查询到的用户
     */
    List<User> findAll();

}
