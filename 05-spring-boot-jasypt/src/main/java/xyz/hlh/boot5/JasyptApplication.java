package xyz.hlh.boot5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HLH
 * @description: 启动类
 * @email 17703595860@163.com
 * @date : Created in 2021/8/19 19:56
 */
@SpringBootApplication
public class JasyptApplication {
    public static void main(String[] args) {
        SpringApplication.run(JasyptApplication.class);
    }
}
