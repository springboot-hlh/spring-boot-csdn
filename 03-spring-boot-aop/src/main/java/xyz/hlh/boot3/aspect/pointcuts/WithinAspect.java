package xyz.hlh.boot3.aspect.pointcuts;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author HLH
 * @description Within的测试
 * @email 17703595860@163.com
 * @date Created in 2021/8/7 上午11:25
 */
//@Component // 注册组件
//@Aspect // 开启aop
public class WithinAspect {

    /**
     * 匹配所有UserService类中的所有方法
     */
    @Pointcut(value = "within(xyz.hlh.boot3.service.UserService)")
    public void pointCut() {}

    /**
     * 匹配所有service包中的所有类中的所有方法
     */
    @Pointcut(value = "within(xyz.hlh.boot3.service.*)")
    public void pointCut1() {}

    /**
     * 匹配所有service包以及所有子包中的所有类中的所有方法
     */
    @Pointcut(value = "within(xyz.hlh.boot3.service..*)")
    public void pointCut2() {}

    @Before(value = "pointCut()")
    public void before(JoinPoint joinPoint) {
        System.out.println("前置通知");
    }

    @AfterReturning(value = "pointCut()", returning = "returnObj")
    public void afterReturning(JoinPoint joinPoint, Object returnObj) {
        System.out.println("后置通知");
    }

    @AfterThrowing(value = "pointCut()", throwing = "e")
    public void afterThrowing(JoinPoint joinPoint, Exception e) {
        System.out.println("异常通知");
    }

    @After(value = "pointCut()")
    public void afterThrowing(JoinPoint joinPoint) {
        System.out.println("最终通知");
    }

}
