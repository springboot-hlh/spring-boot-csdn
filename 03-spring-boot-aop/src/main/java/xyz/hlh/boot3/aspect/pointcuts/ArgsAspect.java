package xyz.hlh.boot3.aspect.pointcuts;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import xyz.hlh.boot3.entity.User;

/**
 * @author HLH
 * @description args的测试
 * @email 17703595860@163.com
 * @date Created in 2021/8/7 上午11:25
 */
//@Component // 注册组件
//@Aspect // 开启aop
public class ArgsAspect {

    /**
     * 匹配所有UserService类中的所有方法
     */
    @Pointcut(value = "within(xyz.hlh.boot3.service.UserService)")
    public void pointCut() {}

    /**
     * args 向切入点中传入参数（匹配参数），argNames指定此方法中的参数名词列表
     *  该测试方法为修改User的方法 入参有两个 User user，Integer userId
     *  args中参数名词随意，但是参数个数需要匹配
     *  此处通知方法中的参数类型可以指定具体的类型，spring自动强转，但是如果类型不对，会造成进不去该通知
     */
    @Before(value = "pointCut() && args(a, b)", argNames = "joinPoint,a,b")
    public void before(JoinPoint joinPoint, User a, Integer b) {
        System.out.println(a);
        System.out.println("前置通知");
    }

    @AfterReturning(value = "pointCut()", returning = "returnObj")
    public void afterReturning(JoinPoint joinPoint, Object returnObj) {
        System.out.println("后置通知");
    }

    @AfterThrowing(value = "pointCut()", throwing = "e")
    public void afterThrowing(JoinPoint joinPoint, Exception e) {
        System.out.println("异常通知");
    }

    @After(value = "pointCut()")
    public void afterThrowing(JoinPoint joinPoint) {
        System.out.println("最终通知");
    }

}
