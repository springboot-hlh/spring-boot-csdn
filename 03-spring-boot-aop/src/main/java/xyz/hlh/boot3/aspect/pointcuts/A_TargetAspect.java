package xyz.hlh.boot3.aspect.pointcuts;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author HLH
 * @description @target的测试
 * @email 17703595860@163.com
 * @date Created in 2021/8/7 上午11:25
 */
//@Component // 注册组件
//@Aspect // 开启aop
public class A_TargetAspect {

    /**
     * @target 用于匹配标书指定注解的类中的所有方法，要求注解必须是RUNTIME的
     *  匹配UserService类，并且标注了MyAnno注解的类中的所有方法
     *  @target 不能单独使用，网上并没有类似说明，可能是我的springboot原因，单独使用会报错。合并使用效果正常
     */
    @Pointcut("within(xyz.hlh.boot3.service.UserService) && @target(xyz.hlh.boot3.annotation.MyAnno)")
    public void pointCut() {}

    @Before(value = "pointCut()")
    public void before(JoinPoint joinPoint) {
        System.out.println("前置通知");
    }

    @AfterReturning(value = "pointCut()", returning = "returnObj")
    public void afterReturning(JoinPoint joinPoint, Object returnObj) {
        System.out.println("后置通知");
    }

    @AfterThrowing(value = "pointCut()", throwing = "e")
    public void afterThrowing(JoinPoint joinPoint, Exception e) {
        System.out.println("异常通知");
    }

    @After(value = "pointCut()")
    public void afterThrowing(JoinPoint joinPoint) {
        System.out.println("最终通知");
    }

}
