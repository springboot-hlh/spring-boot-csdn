package xyz.hlh.boot3.aspect.pointcuts;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import xyz.hlh.boot3.entity.User;

/**
 * @author HLH
 * @description @args的测试
 * @email 17703595860@163.com
 * @date Created in 2021/8/7 上午11:25
 */
//@Component // 注册组件
//@Aspect // 开启aop
public class A_ArgsAspect {

    /**
     * 匹配所有UserService类中的所有方法
     */
    @Pointcut(value = "within(xyz.hlh.boot3.service.UserService)")
    public void pointCut() {}

    /**
     * @args 和args都是匹配参数，但是@args要求传入切入点的参数必须标注指定注解，且不能是SOURCE源码注解，比如Lombok的
     *  该测试方法为修改User的方法 入参有两个 User user，Integer userId
     *  @args中指定对应的标注注解的的类型，参数个数需要匹配，不校验的参数用*代替
     *  该处要求第一个参数User对象必须标注MyAnno的自定义注解
     */
    @Before(value = "pointCut() && @args(xyz.hlh.boot3.annotation.MyAnno, *)")
    public void before(JoinPoint joinPoint) {
        System.out.println("前置通知");
    }

    @AfterReturning(value = "pointCut()", returning = "returnObj")
    public void afterReturning(JoinPoint joinPoint, Object returnObj) {
        System.out.println("后置通知");
    }

    @AfterThrowing(value = "pointCut()", throwing = "e")
    public void afterThrowing(JoinPoint joinPoint, Exception e) {
        System.out.println("异常通知");
    }

    @After(value = "pointCut()")
    public void afterThrowing(JoinPoint joinPoint) {
        System.out.println("最终通知");
    }

}
