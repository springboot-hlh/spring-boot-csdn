package xyz.hlh.boot3.aspect.pointcuts;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author HLH
 * @description bean的测试
 * @email 17703595860@163.com
 * @date Created in 2021/8/7 上午11:25
 */
//@Component // 注册组件
//@Aspect // 开启aop
public class BeanAspect {

    /**
     * 匹配spring容器中为指定beanName的对象中的所有方法
     *  此处匹配spring容器中名称为userService的对象中的所有方法
     */
    @Pointcut(value = "bean(userService)")
    public void pointCut() {}

    /**
     * 匹配spring容器中为指定beanName的对象中的所有方法
     *  此处匹配spring容器中名称为Service结尾的对象中的所有方法
     */
    @Pointcut(value = "bean(*Service)")
    public void pointCut1() {}

    @Before(value = "pointCut()")
    public void before(JoinPoint joinPoint) {
        System.out.println("前置通知");
    }

    @AfterReturning(value = "pointCut()", returning = "returnObj")
    public void afterReturning(JoinPoint joinPoint, Object returnObj) {
        System.out.println("后置通知");
    }

    @AfterThrowing(value = "pointCut()", throwing = "e")
    public void afterThrowing(JoinPoint joinPoint, Exception e) {
        System.out.println("异常通知");
    }

    @After(value = "pointCut()")
    public void afterThrowing(JoinPoint joinPoint) {
        System.out.println("最终通知");
    }

}
