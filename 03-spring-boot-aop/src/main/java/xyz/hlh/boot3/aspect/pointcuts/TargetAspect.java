package xyz.hlh.boot3.aspect.pointcuts;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author HLH
 * @description target的测试
 * @email 17703595860@163.com
 * @date Created in 2021/8/7 上午11:25
 */
//@Component // 注册组件
//@Aspect // 开启aop
public class TargetAspect {

    /**
     * 匹配所有UserService类中的所有方法
     */
    @Pointcut(value = "within(xyz.hlh.boot3.service.UserService)")
    public void pointCut() {}

    /**
     * target 向切入点中传入目标对象，argNames指定此方法中的参数名词列表
     */
    @Before(value = "pointCut() && target(obj)", argNames = "joinPoint,obj")
    public void before(JoinPoint joinPoint, Object obj) {
        // 目标对象
        System.out.println(obj);
        System.out.println("前置通知");
    }

    @AfterReturning(value = "pointCut()", returning = "returnObj")
    public void afterReturning(JoinPoint joinPoint, Object returnObj) {
        System.out.println("后置通知");
    }

    @AfterThrowing(value = "pointCut()", throwing = "e")
    public void afterThrowing(JoinPoint joinPoint, Exception e) {
        System.out.println("异常通知");
    }

    @After(value = "pointCut()")
    public void afterThrowing(JoinPoint joinPoint) {
        System.out.println("最终通知");
    }

}
