package xyz.hlh.boot3.controller;

import org.springframework.web.bind.annotation.*;
import xyz.hlh.boot3.entity.User;
import xyz.hlh.boot3.service.UserService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author HLH
 * @description
 * @email 17703595860@163.com
 * @date Created in 2021/8/7 上午10:39
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping
    public List<User> findAll() {
        return userService.findAll();
    }

    @PostMapping
    public User insertUser(@RequestBody User user) {
        return userService.insertUser(user);
    }

    @PutMapping("/{userId}")
    public User updateUser(@RequestBody User user, @PathVariable Integer userId) {
        return userService.updateUser(user, userId);
    }

    @DeleteMapping("/{userId}")
    public void deleteUser(@PathVariable Integer userId) {
        userService.deleteUser(userId);
    }

}
