package xyz.hlh.boot3.exception;

/**
 * @author HLH
 * @description 自定义异常
 * @email 17703595860@163.com
 * @date Created in 2021/8/7 上午11:23
 */
public class MyException extends RuntimeException {

    private static final long serialVersionUID = -3140948886577939420L;

    public MyException(String message) {
        super(message);
    }
}
