package xyz.hlh.boot3.annotation;

import java.lang.annotation.*;

/**
 * @author HLH
 * @description 自定义注解
 * @email 17703595860@163.com
 * @date Created in 2021/8/8 下午6:54
 */
@Target({ElementType.TYPE, ElementType.METHOD})  // 作用于类
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyAnno {
}
