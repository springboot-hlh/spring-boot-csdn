package xyz.hlh.boot8.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import xyz.hlh.boot8.common.Result;

/**
 * @author HLH
 * @description:
 * @email 17703595860@163.com
 * @date : Created in 2021/11/21 11:36
 */
@RestControllerAdvice
public class GlobExceptionHandler {

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<?> customExceptionHanler(CustomException e) {
        return ResponseEntity.status(e.getCode())
                .body(Result.builder().code(e.getCode()).errorMsg(e.getMessage()).build());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> exceptionHanler(Exception e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(Result.builder().code(HttpStatus.INTERNAL_SERVER_ERROR.value()).errorMsg(e.getMessage()).build());
    }

}
