package xyz.hlh.boot8.exception;

import lombok.Getter;

/**
 * @author HLH
 * @description:
 * @email 17703595860@163.com
 * @date : Created in 2021/11/21 11:37
 */
public class CustomException extends RuntimeException {

    @Getter
    private final int code;

    public CustomException(int code, String message) {
        super(message);
        this.code = code;
    }
}
