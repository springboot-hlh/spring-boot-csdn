package xyz.hlh.boot8.config;

import com.github.tobato.fastdfs.FdfsClientConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author HLH
 * @description:
 * @email 17703595860@163.com
 * @date : Created in 2021/11/20 10:59
 */
@Configuration
@Import({FdfsClientConfig.class})
public class AppConfig {
}
