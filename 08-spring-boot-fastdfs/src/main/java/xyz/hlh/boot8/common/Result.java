package xyz.hlh.boot8.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author HLH
 * @description: 统一返回值
 * @email 17703595860@163.com
 * @date : Created in 2021/11/21 11:34
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Result<T> implements Serializable {

    private int code;
    private T data;
    private String errorMsg;

}
