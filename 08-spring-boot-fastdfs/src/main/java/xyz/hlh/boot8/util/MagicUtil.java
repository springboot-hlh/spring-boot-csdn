package xyz.hlh.boot8.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author HLH
 * @description: 文件类型判断工具类
 * 根据文件类型魔数 判断文件真实类型
 * @email 17703595860@163.com
 * @date : Created in 2021/11/20 11:00
 */
@Service
public class MagicUtil {

    private Map<String, String> FILE_TYPE_MAP = new HashMap<>();
    private List<String> imgSuffixList = Arrays.asList(
            ".jpg",".png",".gif",".tif",".bmp"
    );

    @PostConstruct
    public void setFileTypeMap() {
        // 微软 office
        FILE_TYPE_MAP.put("d0cf11e0a1b11ae10000", ".doc,.ppt,.xlsx"); // MS Excel 97-2003 注意：word、msi 和 excel的文件头一样
        FILE_TYPE_MAP.put("504b0304140006000800", ".docx,.pptx,.xlsx"); // MS Excel 97-2003+
        // 前端 java
        FILE_TYPE_MAP.put("3c21444f435459504520", ".html"); // HTML (html)
        FILE_TYPE_MAP.put("48544d4c207b0d0a0942", ".css"); // css
        FILE_TYPE_MAP.put("696b2e71623d696b2e71", ".js"); // js
        FILE_TYPE_MAP.put("3c25402070616765206c", ".jsp");// jsp文件
        FILE_TYPE_MAP.put("3c3f786d6c2076657273", ".xml");// xml文件
        FILE_TYPE_MAP.put("494e5345525420494e54", ".sql");// sql文件
        FILE_TYPE_MAP.put("7061636b616765207765", ".java");// java文件
        FILE_TYPE_MAP.put("cafebabe0000002e00", ".class");
        FILE_TYPE_MAP.put("cafebabe0000002e0041", ".class");
        // 图片
        FILE_TYPE_MAP.put("ffd8ffe000104a464946", ".jpg"); // JPEG (jpg)
        FILE_TYPE_MAP.put("89504e470d0a1a0a0000", ".png"); // PNG (png)
        FILE_TYPE_MAP.put("47494638396126026f01", ".gif"); // GIF (gif)
        FILE_TYPE_MAP.put("49492a00227105008037", ".tif"); // TIFF (tif)
        FILE_TYPE_MAP.put("424d228c010000000000", ".bmp"); // 16色位图(bmp)
        FILE_TYPE_MAP.put("424d8240090000000000", ".bmp"); // 24位位图(bmp)
        FILE_TYPE_MAP.put("424d8e1b030000000000", ".bmp"); // 256色位图(bmp)
        // pdf
        FILE_TYPE_MAP.put("255044462d312e350d0a", ".pdf"); // Adobe Acrobat (pdf)
        // 影音
        FILE_TYPE_MAP.put("464c5601050000000900", ".flv,.f4v"); // flv与f4v相同
        FILE_TYPE_MAP.put("00000020667479706d70", ".mp4");
        FILE_TYPE_MAP.put("49443303000000002176", ".mp3");
        FILE_TYPE_MAP.put("000001ba210001000180", ".mpg"); //
        FILE_TYPE_MAP.put("3026b2758e66cf11a6d9", ".wmv,.asf"); // wmv与asf相同
        FILE_TYPE_MAP.put("57415645", "wav");
        FILE_TYPE_MAP.put("41564920", "avi");
        // 压缩包
        FILE_TYPE_MAP.put("504b0304140000000800", "zip");
        FILE_TYPE_MAP.put("526172211a0700cf9073", "rar");
        FILE_TYPE_MAP.put("1f8b0800000000000000", "gz");// gz文件
        // 其他
        FILE_TYPE_MAP.put("38425053000100000000", ".psd"); // Photoshop (psd)
        FILE_TYPE_MAP.put("4d5a9000030000000400", "exe");// 可执行文件
        FILE_TYPE_MAP.put("41433130313500000000", ".dwg"); // CAD (dwg)
        FILE_TYPE_MAP.put("5374616E64617264204A", ".mdb"); // MS Access (mdb)
        FILE_TYPE_MAP.put("7b5c727466315c616e73", ".rtf"); // Rich Text Format (rtf)
        FILE_TYPE_MAP.put("46726f6d3a203d3f6762", ".eml"); // Email [Outlook Express 6] (eml)
        FILE_TYPE_MAP.put("252150532D41646F6265", ".ps");
        FILE_TYPE_MAP.put("2e524d46000000120001", ".rmvb,.rm"); // rmvb/rm相同
        FILE_TYPE_MAP.put("4d546864000000060001", "mid"); // MIDI (mid)
        FILE_TYPE_MAP.put("235468697320636f6e66", "ini");
        FILE_TYPE_MAP.put("4d616e69666573742d56", "mf");// MF文件
        FILE_TYPE_MAP.put("406563686f206f66660d", "bat");// bat文件
        FILE_TYPE_MAP.put("6c6f67346a2e726f6f74", "properties");// bat文件
        FILE_TYPE_MAP.put("49545346030000006000", "chm");// bat文件
        FILE_TYPE_MAP.put("04000000010000001300", "mxp");// bat文件
        FILE_TYPE_MAP.put("6431303a637265617465", "torrent");
        FILE_TYPE_MAP.put("6D6F6F76", "mov"); // Quicktime (mov)
        FILE_TYPE_MAP.put("FF575043", "wpd"); // WordPerfect (wpd)
        FILE_TYPE_MAP.put("CFAD12FEC5FD746F", "dbx"); // Outlook Express (dbx)
        FILE_TYPE_MAP.put("2142444E", "pst"); // Outlook (pst)
        FILE_TYPE_MAP.put("AC9EBD8F", "qdf"); // Quicken (qdf)
        FILE_TYPE_MAP.put("E3828596", "pwl"); // Windows Password (pwl)
        FILE_TYPE_MAP.put("2E7261FD", "ram"); // Real Audio (ram)
    }

    /**
     * 根据文件获取文件的真实类型
     *
     * @param file 文件
     * @return 类型列表
     */
    public List<String> getSuffixListByMagic(MultipartFile file) {
        String fileMagic = getFileMagic(file);
        if (StringUtils.isBlank(fileMagic)) {
            return new ArrayList<>();
        }
        return getSuffixListByMagic(fileMagic);
    }

    /**
     * 根据文件流的前几位 魔数
     *  获取文件的真实类型列表   因为一个魔数可能对应多种类型
     *
     * @param magic 文件魔数
     * @return 类型列表
     */
    public List<String> getSuffixListByMagic(String magic) {
        String fileTypeSuffixString = null;
        for (Map.Entry<String, String> entry : FILE_TYPE_MAP.entrySet()) {
            if (entry.getKey().toLowerCase().startsWith(magic.toLowerCase()) || magic.toLowerCase().startsWith(entry.getKey().toLowerCase())) {
                fileTypeSuffixString = entry.getValue();
                break;
            }
        }
        if (StringUtils.isBlank(fileTypeSuffixString)) {
            return new ArrayList<>();
        }
        return Arrays.stream(StringUtils.split(fileTypeSuffixString, ","))
                .map(e -> !StringUtils.equals(e.substring(0, 1), ".") ? "." + e : e)
                .collect(Collectors.toList());
    }

    /**
     * 根据文件 获取文件魔数
     * 从输入流中读取28个字节数据，是因为不同格式的文件头魔数长度是不一样的
     * 为了提高识别精度所以获取的字节数相应地长一点
     *
     * @param file 文件
     * @return 文件魔数
     */
    public String getFileMagic(MultipartFile file) {
        try (InputStream is = file.getInputStream();) {
            /*FileInputStream is = (FileInputStream) inputStream;*/
            byte[] b = new byte[28];
            is.read(b, 0, b.length);
            // 得到上传文件十六进制文件头
            StringBuilder stringBuilder = new StringBuilder();
            if (b == null || b.length <= 0) {
                return null;
            }
            for (byte value : b) {
                int v = value & 0xFF;
                String hv = Integer.toHexString(v);
                if (hv.length() < 2) {
                    stringBuilder.append(0);
                }
                stringBuilder.append(hv);
            }
            return stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 判断文件是否是图片类型
     * @param fileSuffixList 文件真实后缀列表
     * @return boolean
     */
    public boolean isImg(List<String> fileSuffixList) {
        return CollectionUtils.containsAny(fileSuffixList, imgSuffixList);
    }

    /**
     * 判断文件是否是图片类型
     * @param fileSuffix 文件真实后缀
     * @return boolean
     */
    public boolean isImg(String fileSuffix) {
        return imgSuffixList.contains(fileSuffix);
    }

}
