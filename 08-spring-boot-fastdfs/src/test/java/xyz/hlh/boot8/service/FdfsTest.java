package xyz.hlh.boot8.service;

import com.github.tobato.fastdfs.domain.fdfs.MetaData;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.domain.fdfs.ThumbImageConfig;
import com.github.tobato.fastdfs.domain.proto.storage.DownloadCallback;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FdfsTest {

    @Autowired
    private FastFileStorageClient storageClient;

    @Autowired
    private ThumbImageConfig thumbImageConfig;

    /**
     * 下载文件
     * @throws IOException
     */
    @Test
    public void downloadTest() throws IOException {
        InputStream is = storageClient.downloadFile("group1", "M00/00/00/wKgBC2GYd5-AKGCyAAFRCK-PapQ167.jpg", new DownloadCallback<InputStream>() {
            @Override
            public InputStream recv(InputStream ins) throws IOException {
                return ins;
            }
        });
        FileOutputStream os = new FileOutputStream("D:\\桌面\\test1.jpg");
        FileCopyUtils.copy(is, os);
        is.close();
        os.close();
    }

    /**
     * 获取文件的元数据信息
     * @throws IOException
     */
    @Test
    public void getMetadata() throws IOException {
        Set<MetaData> metadataSet = storageClient.getMetadata("group1", "M00/00/00/wKgBC2GYiSSAdKt6AAFRCK-PapQ695.png");
        System.out.println(metadataSet);
    }

    /**
     * 上传文件
     */
    @Test
    public void testUpload() throws FileNotFoundException {
        File file = new File("D:\\桌面\\test.jpg");
        // 上传文件
        StorePath storePath = this.storageClient.uploadFile(
                new FileInputStream(file), file.length(), "jpg", null);
        // 带分组的路径
        System.out.println(storePath.getFullPath());
        // 不带分组的路径
        System.out.println(storePath.getPath());
    }

    /**
     * 上传图片并且生成缩略图
     * @throws FileNotFoundException
     */
    @Test
    public void testUploadAndCreateThumb() throws FileNotFoundException {
        File file = new File("D:\\桌面\\test.jpg");
        // 把原文件名作为元数据上传
        Set<MetaData> metadataSet = new HashSet<>();
        metadataSet.add(new MetaData("oldFieldName", "test.jpg"));
        // 上传并且生成缩略图
        StorePath storePath = this.storageClient.uploadImageAndCrtThumbImage(
                new FileInputStream(file), file.length(), "png", metadataSet);
        // 带分组的路径
        System.out.println(storePath.getFullPath());
        // 不带分组的路径
        System.out.println(storePath.getPath());
        // 获取缩略图路径
        String path = thumbImageConfig.getThumbImagePath(storePath.getPath());
        System.out.println(path);
    }
}