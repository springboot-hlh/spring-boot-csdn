package xyz.hlh.annotition;

import java.lang.annotation.*;

/**
 * @description 登录校验注解，用户aop校验
 * @author HLH
 * @email 17703595860@163.com
 * @date Created in 2021/8/1 下午9:35
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LoginValidator {

    boolean validated() default true;

}
