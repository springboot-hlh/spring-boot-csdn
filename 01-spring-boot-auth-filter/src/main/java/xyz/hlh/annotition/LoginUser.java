package xyz.hlh.annotition;

import java.lang.annotation.*;

/**
 * @description 登录参数注解，通过spring参数解析器解析
 * @author HLH
 * @email 17703595860@163.com
 * @date Created in 2021/8/1 下午9:35
 */
@Target(ElementType.PARAMETER)  // 作用于参数
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LoginUser {

}
