package xyz.hlh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * @author HLH
 * @email 17703595860@163.com
 * @date : Created in  2021/8/1 下午4:40
 */
@SpringBootApplication
@ServletComponentScan
public class AuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }
}
