package xyz.hlh.common;

/**
 * @description 常量类
 * @author HLH
 * @email 17703595860@163.com
 * @date Created in  2021/8/1 下午8:25
 */
public class Constant {

    /** redis存储的key的前缀 */
    public static final String REDIS_USER_PREFIX = "user:";
    /** token存储的请求头名称 */
    public static final String TOKEN_HEADER_NAME = "Authorization";


}
