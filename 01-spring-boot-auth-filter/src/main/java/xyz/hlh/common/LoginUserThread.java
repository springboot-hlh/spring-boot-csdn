package xyz.hlh.common;

import xyz.hlh.entity.User;

/**
 * @author HLH
 * @description 线程池变量
 * @email 17703595860@163.com
 * @date Created in 2021/8/7 下午7:35
 */
public class LoginUserThread {

    /** 线程池变量 */
    private static final ThreadLocal<User> LOGIN_USER = new ThreadLocal<>();

    private LoginUserThread() {}

    public static User get() {
        return LOGIN_USER.get();
    }

    public static void put(User user) {
        LOGIN_USER.set(user);
    }

    public static void remove() {
        LOGIN_USER.remove();
    }

}
