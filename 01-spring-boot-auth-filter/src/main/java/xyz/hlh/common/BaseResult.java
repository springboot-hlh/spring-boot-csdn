package xyz.hlh.common;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * @description 提供构造统一返回结果
 * @author HLH
 * @email 17703595860@163.com
 * @date : Created in  2021/8/1 下午5:04
 */
public interface BaseResult {

    /**
     * 构造成功之后的返回对象
     * @param data 返回对象
     * @return ResponseEntity<Result<?>>
     */
    default ResponseEntity<Result<?>> success(Object data) {
        return ResponseEntity.ok(
                Result.builder().code(HttpStatus.OK.value()).data(data).build()
        );
    }

    /**
     * 构造失败之后的返回对象
     * @param errorMsg 错误信息
     * @return ResponseEntity<Result<?>>
     */
    default ResponseEntity<Result<?>> internalError(String errorMsg) {
        return ResponseEntity.ok(
                Result.builder().code(HttpStatus.INTERNAL_SERVER_ERROR.value()).errorMsg(errorMsg).build()
        );
    }

}
