package xyz.hlh.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description 统一结果返回类
 * @author HLH
 * @email 17703595860@163.com
 * @date : Created in  2021/8/1 下午5:02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 9110032459932964235L;

    private int code;
    private String errorMsg;
    private T data;

}
