package xyz.hlh.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import xyz.hlh.argumentResolvers.LoginUserResolver;
import xyz.hlh.filter.LoginFilter;
import xyz.hlh.interception.LoginInterception;

import javax.annotation.Resource;
import javax.servlet.Filter;
import java.util.List;

/**
 * @description webMvc 配置类
 * @author HLH
 * @email 17703595860@163.com
 * @date Created in 2021/8/1 下午8:34
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Resource
    private LoginProperties loginProperties;
    @Resource
    private LoginInterception loginInterception;
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Resource
    private LoginUserResolver loginUserResolver;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(loginUserResolver);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterception)
                .addPathPatterns(loginProperties.getInterceptorIncludeUrl())
                .excludePathPatterns(loginProperties.getInterceptorExcludeUrl());
    }

    /*
     * 添加过滤器
     */
    /*@Bean
    public FilterRegistrationBean<Filter> loginFilterRegistration() {
        // 注册LoginFilter
        FilterRegistrationBean<Filter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new LoginFilter(redisTemplate, loginProperties));
        // 设置名称
        registrationBean.setName("loginFilter");
        // 设置拦截路径
        registrationBean.addUrlPatterns(loginProperties.getFilterIncludeUrl().toArray(new String[0]));
        // 指定顺序
        registrationBean.setOrder(-1);
        return registrationBean;
    }*/

}
