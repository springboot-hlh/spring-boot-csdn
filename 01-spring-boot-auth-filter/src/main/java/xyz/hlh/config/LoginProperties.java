package xyz.hlh.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * @description 登陆拦截路径
 * @author HLH
 * @email 17703595860@163.com
 * @date : Created in  2021/8/1 下午8:17
 */
@Component
@ConfigurationProperties(prefix = "login")
@Data
public class LoginProperties implements Serializable {
    private static final long serialVersionUID = 7052116364833723328L;

    private List<String> filterIncludeUrl;
    private List<String> filterExcludeUrl;

    private List<String> interceptorIncludeUrl;
    private List<String> interceptorExcludeUrl;

}
