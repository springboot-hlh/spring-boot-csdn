package xyz.hlh.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description 常量类
 * @author HLH
 * @email 17703595860@163.com
 * @date : Created in  2021/8/1 下午4:44
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private static final long serialVersionUID = -8747566992226024213L;

    private Integer id;
    private String username;
    private String password;
    private Boolean enable;

}
