package xyz.hlh.boot6.jdbc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import xyz.hlh.boot6.jdbc.util.SqlQuery;

/**
 * @author HLH
 * @description: SqlQuery的自动配置类
 * @email 17703595860@163.com
 * @date : Created in 2021/10/12 10:06
 */
@Configuration
public class SqlQueryAutoConfiguration {

    @Bean
    public SqlQuery sqlQuery() {
        return new SqlQuery();
    }

}
