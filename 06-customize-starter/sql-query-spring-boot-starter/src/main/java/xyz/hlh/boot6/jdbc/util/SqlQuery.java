package xyz.hlh.boot6.jdbc.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * @author HLH
 * @description: jdbcTemplate的封装 SqlQuery
 * @email 17703595860@163.com
 * @date : Created in 2021/10/12 10:06
 */
public class SqlQuery {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 查询所有数据
     * @return 数据列表
     */
    public <T> List<T> findEntityList(String sql, Class<T> tClass, Object ... args) {
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(tClass), args);
    }


    /**
     * 查询单挑数据
     * @return 数据
     */
    public <T> T findEntity(String sql, Class<T> tClass, Object ... args) {
        return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(tClass), args);
    }

}
