package xyz.hlh.boot6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author HLH
 * @description: 启动器
 * @email 17703595860@163.com
 * @date : Created in 2021/10/12 10:31
 */
@SpringBootApplication
public class Boot6Application {
    public static void main(String[] args) {
        SpringApplication.run(Boot6Application.class, args);
    }
}
