package xyz.hlh.boot6.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author HLH
 * @description: 用户实体类
 * @email 17703595860@163.com
 * @date : Created in 2021/10/12 10:31
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User implements Serializable {

    private long id;
    private String username;
    private String password;
    private String email;
    private String city;
    private Boolean isDelete;

}
