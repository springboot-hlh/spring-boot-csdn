package xyz.hlh.boot6.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.hlh.boot6.anno.MyLog;
import xyz.hlh.boot6.entity.User;
import xyz.hlh.boot6.jdbc.util.SqlQuery;

import java.util.List;

/**
 * @author HLH
 * @description: 用户Controller
 * @email 17703595860@163.com
 * @date : Created in 2021/10/12 11:02
 */
@RestController
@RequestMapping("/user")
@MyLog
public class UserController {

    @Autowired
    private SqlQuery sqlQuery;

    @GetMapping
    public List<User> findAllUser() {
        List<User> userList = sqlQuery.findEntityList("select * from user", User.class, null);
        return userList;
    }

    @GetMapping("/{id}")
    @MyLog(false)
    public User findUser(@PathVariable Long id) {
        User user = sqlQuery.findEntity("select * from user where id = ?", User.class, id);
        return user;
    }

}
