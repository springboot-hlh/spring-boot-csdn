package xyz.hlh.boot6.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author HLH
 * @description: 自定义日志注解，标注注解的类或者方法统计日志
 * @email 17703595860@163.com
 * @date : Created in 2021/10/12 12:34
 */
@Target({ElementType.TYPE, ElementType.METHOD})  // 作用于类或者方法
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyLog {

    /** 是否打印日志 */
    boolean value() default true;

}
