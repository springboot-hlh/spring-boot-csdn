package xyz.hlh.boot6.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author HLH
 * @description: log自动配置
 * @email 17703595860@163.com
 * @date : Created in 2021/10/12 10:06
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "xyz.hlh.boot6.log")
public class LogAutoConfiguration {

}
